require 'yaml'

module Nesta
  module Plugin
    module View::Count
      module Helpers
        def index_by_most_often(limit=nil)
          views = YAML.load_file('views.yml') || {}
          sorted_views = views.map do |k,v|
            v.merge({ 'page' => Page.find_by_path(k) })
          end.sort do |a, b|
            (b['views'] || 0) <=> (a['views'] || 0)
          end
          limited = limit ? sorted_views[0..(limit - 1)] : sorted_views
          haml_tag :ul, class: 'top' do
            limited.each do |view_entry|
              page = view_entry['page']
              haml_tag :li do
                haml_concat view_entry['views'] || '0'
                haml_tag :a, :<, href: path_to(page.abspath) do
                  haml_concat page.link_text
                end
              end
            end
          end
        end

        def index_by_most_recent(limit=nil)
          views = YAML.load_file('views.yml') || {}
          sorted_views = views.map do |k,v|
            v.merge({ 'page' => Page.find_by_path(k) })
          end.sort do |a, b|
            (b['last'] || '2018-01-01') <=> (a['last'] || '2018-01-01')
          end
          haml_tag :ul, class: 'top' do
            limited = limit ? sorted_views[0..(limit - 1)] : sorted_views
            limited.each do |view_entry|
              page = view_entry['page']
              haml_tag :li do
                haml_concat view_entry['last'] || 'not viewed yet'
                haml_tag :a, :<, href: path_to(page.abspath) do
                  haml_concat page.link_text
                end
              end
            end
          end
        end
      end
    end
  end

  class App
    helpers Nesta::Plugin::View::Count::Helpers

    IGNORE_LIST = %w(index)

    after do
      @page.record_view unless @page.nil?
    end
  end

  class Page

    def self.find_filtered
      find_all.reject do |p|
        p.ignored?
      end
    end

    def ignored?
      App::IGNORE_LIST.each do |ignore|
        return true if filename =~ /\/?#{ignore}\.\w+$/
      end
      return false
    end

    def record_view
      return if ignored?
      views = YAML.load_file('views.yml') || {}
      page_views = views[path] || {}
      page_views['views'] = (page_views['views'].to_i || 0) + 1
      page_views['first'] = Date.today.strftime('%Y-%m-%d') unless page_views['first']
      page_views['last'] = Date.today.strftime('%Y-%m-%d')

      @metadata['views'] = page_views['views']
      @metadata['first_viewed'] = page_views['first']
      @metadata['last_viewed'] = page_views['last']

      views[path] = page_views
      File.open('views.yml', 'w') { |file| file.write(views.to_yaml) }
    end
  end
end

class Hash
  def to_yaml opts={}
    puts "my to_yaml"
    return Psych.dump(self.clone.sort.to_h)
  end
end
