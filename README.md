# readme
Adds metadata support for view count and last viewed date in a [Nesta](http://nestacms.com) project.

## installation
To use this plugin just add it to your Nesta project's `Gemfile`:

    gem 'nesta-plugin-view-count', :git => 'git@gitlab.com:adamlawr/nesta-plugin-view-count.git'

and then install it with Bundler:

    $ bundle

## usage
add a reference to `:views` and/or `:last_viewed` from the page metadata somewhere in your view file.
for example: `page_meta.haml`

    .metadata
      last viewed:
      = @page.metadata(:last_viewed)
      <br>views:
      = @page.metadata(:views).to_i + 1

__note:__ you can increment the displayed view count by one to be more accurate because the metadata is updated in an after filter. ie after the page has been rendered.
