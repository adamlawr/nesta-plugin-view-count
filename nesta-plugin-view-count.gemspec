# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "nesta-plugin-view-count/version"

Gem::Specification.new do |spec|
  spec.name        = "nesta-plugin-view-count"
  spec.version     = Nesta::Plugin::View::Count::VERSION
  spec.authors     = ["adam lawrence"]
  spec.email       = ["adam@raceweb.ca"]
  spec.homepage    = "https://gitlab.com/adamlawr/nesta-plugin-view-count"
  spec.summary     = %q{add view counts to nesta page metadata}
  spec.description = %q{add view counts and last viewed date to nesta page metadata}
  spec.license     = "MIT"

  spec.rubyforge_project = "nesta-plugin-view-count"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  # specify any dependencies here; for example:
  # spec.add_development_dependency "rspec"
  # spec.add_runtime_dependency "rest-client"
  spec.add_runtime_dependency 'nesta', '~> 0.9', '>= 0.9.11'
  spec.add_development_dependency 'rake', '~> 0'
end
